/** \defgroup char_count CHAR Subsys
 * Counts the number of characters received on the terminal
 * 
 * Tracks numeric digits, small letters, capital letters, and total characters
 * received over the subsystem UART channel.
 * 
 * #### Commands ####
 * - `$char reset`
 *   - Reset the count to 0  
 * - `$char print`
 *   - Print the current count  
 * - `$char help`
 *   - Show the help text
 * 
 * \pre    Include the following Embeddedlibrary modules in system.h:
 *           - Subsystem
 *           - UART
 *           - Task
 * \pre    Call Char_Init() to initialize the subsystem
 * 
 * \author Josh Klodnicki
 * @{
 */

#ifndef CHAR_COUNT_H
#define	CHAR_COUNT_H

/** Initializes the CHAR Subsystem
 * 
 * \author Josh Klodnicki
 */
void Char_Init(void);

#endif	/* CHAR_COUNT_H */

///@}

