README
======

Klodnicki Embedded Library
--------------------------

A library of modules for embedded systems written by Josh Klodnicki
<<joshklod@gmail.com>>.

### Modules ###
- \ref one_wire_midi
- \ref ascii_table
- \ref average32
- \ref char_count
- \ref keyboard_scale
- \ref rgb_colors
- \ref tetris

This library is hosted
[on BitBucket](https://bitbucket.org/joshklod/klodnicki-embedded-library
"Klodnicki Embedded Library").