/** \ingroup tetris
 * @{
 */
 
#include "system.h"
#include "tetris.h"

/* Default Macro Settings */

#ifndef PLAYER1_UART
/** The UART Channel connected to the player's terminal */
#define PLAYER1_UART SUBSYS_UART
#endif

/* Other Macros */

#define SCREENWIDTH  10
#define SCREENHEIGHT 20

/* Derived Macros */

// The horizontal center of the screen
#define CENTER       (SCREENWIDTH - 1)/2

/* File-Scope Variables */

/** Represents a single square of a tetris piece.
 * Each piece consists of four of these x/y-coordinate pairs.
 */
typedef struct {
    uint8_t x;
    uint8_t y;
} tet_block_t;

static uint8_t game_id;
// The current tetris piece (persistent between function calls)
static tet_block_t blocks[4];
// The time between automatic downward movements in milliseconds
static uint16_t speed;

// The tiles at the bottom of the screen that are occupied by a piece
#if SCREENWIDTH <= 8
static uint8_t  full_tiles[SCREENHEIGHT];
#elif SCREENWIDTH <= 16
static uint16_t full_tiles[SCREENHEIGHT];
#elif SCREENWIDTH <= 32
static uint32_t full_tiles[SCREENHEIGHT];
#else
static uint64_t full_tiles[SCREENHEIGHT];
#endif

/* Function Prototypes */

/** Callback handling extra subsys commands
 *
 * \warning The master branch of the Embeddedlibrary game module does not pass
 *          these arguments correctly as of 5/11/2016
 */
static void callback(int argc, char *argv[]);

/** Displays an 'unrecognized command' error message on the terminal.
 * \param str The bad command that was entered by the user
 */
static void error_msg(char *str);

/** Prints the help info to the terminal */
static void show_help(void);

/** Starts a game of Tetris. */
static void start_game(void);

/** Restarts the game. (duh) */
static void restart_game(void);

/** Char receiver callback to handle keypresses ingame.
 * \param c The key pressed by the player
 */
static void receiver(char c);

/** Quits the current game and returns to the command line. */
static void exit_game(void);

/** Generates a random new Tetris block at the top of the screen. */
static void new_block(void);

/** Draws a square consisting of two ASCII block characters.
 * \param x x-coordinate of the square
 * \param y y-coordinate of the square
 */
static void draw_square(uint8_t x, uint8_t y);

/** Clears a square using two ASCII space characters.
 * \param x x-coordinate of the square
 * \param y y-coordinate of the square
 */
static void draw_blank(uint8_t x, uint8_t y);

/** Moves the piece down the screen and checks whether it has reached the
 *  bottom.
 * This is the function scheduled to facilitate automatic falling of the
 * pieces.
 */
static void step_down(void);

/** Handles the series of events that occur when a piece hits the bottom.
 * 
 * 1. Freezes the piece in place
 * 2. Checks for and clears any full rows
 * 3. Shifts all higher rows down
 * 4. Spawns a new piece
 *
 * \todo Add scoring
 * \bug  A piece occasionally appears in the middle of the screen during a
 *       row clear
 */
static void lock_in(void);

/* Function Definitions */

void tetris_init(void) {
    game_id = Game_Register("Tetris",
            "The relentless building block video puzzle.", start_game,
            show_help);
    Game_RegisterCallback(game_id, callback);
}

/// \todo Allow changing of screen width on command line
static void callback(int argc, char *argv[]) {
    enum {EXIT, DEFAULT} mode;
    // Get mode from first argument
    if(argc) {
        if      (strcasecmp(argv[0], "quit") == 0) mode = EXIT;
        else if (strcasecmp(argv[0], "exit") == 0) mode = EXIT;
        else                                       mode = DEFAULT;
    }
    
    // Process command
    if(argc) {
        switch (mode) {
            case EXIT:
                exit_game();
                break;
            case DEFAULT:
                error_msg(argv[0]);
                break;
            default:
                Game_Log(game_id, "Error processing command.");
        }
    } else {
        show_help();
    }
}

static void error_msg(char *str) {
    Game_Printf("Unrecognized command \"%s\".\r\n", str);
    Game_Printf("Use \"$game help\" to show a list of available "
            "commands.\r\n");
}

/// \todo Fill this placeholder
static void show_help(void) {
    Game_Printf("Showing Help...\r\n");
}

static void start_game(void) {
    Game_ClearScreen();
    Game_HideCursor();
    Game_Printf("Starting Tetris...\r\n");
    Game_DrawRect(0, 1, 2*SCREENWIDTH + 1, SCREENHEIGHT + 2);
    Game_RegisterPlayer1Receiver(receiver);
    
    speed = 1000;
    uint8_t i;
    for (i=SCREENHEIGHT; i; i--)
        full_tiles[i] = 0;
    new_block();
    Task_Schedule(step_down, 0, speed, speed);
}

static void receiver(char c) {
    switch (c) {
        case 'q': exit_game();          break;
        case 'r': restart_game();       break;
        case 'j': tetris_move(LEFT);    break;
        case 'l': tetris_move(RIGHT);   break;
        case 'k': tetris_move(DOWN);    break;
        case 'd': tetris_rotate(LEFT);  break;
        case 'f': tetris_rotate(RIGHT); break;
#ifdef CHEAT
        case 'i': tetris_move(UP);      break;
#endif
    }
}

static void restart_game(void) {
    exit_game();
    start_game();
}

static void exit_game(void) {
    Game_ClearScreen();
    Game_ShowCursor();
    Game_Printf("Quitting...\r\n");
    Game_UnregisterPlayer1Receiver(receiver);
    Task_Remove(step_down, 0);
}

/// \todo Next block preview
static void new_block(void) {
    enum {I, J, L, O, S, T, Z} shape;
    shape = random_int(0, 6);
    
    switch (shape) {
        case I:
            blocks[0] = (tet_block_t){CENTER    , 0};
            blocks[1] = (tet_block_t){CENTER - 1, 0};
            blocks[2] = (tet_block_t){CENTER + 1, 0};
            blocks[3] = (tet_block_t){CENTER + 2, 0};
            break;
        case J:
            blocks[0] = (tet_block_t){CENTER    , 0};
            blocks[1] = (tet_block_t){CENTER - 1, 0};
            blocks[2] = (tet_block_t){CENTER + 1, 0};
            blocks[3] = (tet_block_t){CENTER + 1, 1};
            break;
        case L:
            blocks[0] = (tet_block_t){CENTER    , 0};
            blocks[1] = (tet_block_t){CENTER - 1, 1};
            blocks[2] = (tet_block_t){CENTER - 1, 0};
            blocks[3] = (tet_block_t){CENTER + 1, 0};
            break;
        case O:
            blocks[0] = (tet_block_t){CENTER    , 0};
            blocks[1] = (tet_block_t){CENTER + 1, 0};
            blocks[2] = (tet_block_t){CENTER + 1, 1};
            blocks[3] = (tet_block_t){CENTER    , 1};
            break;
        case S:
            blocks[0] = (tet_block_t){CENTER    , 0};
            blocks[1] = (tet_block_t){CENTER - 1, 1};
            blocks[2] = (tet_block_t){CENTER    , 1};
            blocks[3] = (tet_block_t){CENTER + 1, 0};
            break;
        case T:
            blocks[0] = (tet_block_t){CENTER    , 0};
            blocks[1] = (tet_block_t){CENTER - 1, 0};
            blocks[2] = (tet_block_t){CENTER + 1, 0};
            blocks[3] = (tet_block_t){CENTER    , 1};
            break;
        case Z:
            blocks[0] = (tet_block_t){CENTER    , 0};
            blocks[1] = (tet_block_t){CENTER - 1, 0};
            blocks[2] = (tet_block_t){CENTER    , 1};
            blocks[3] = (tet_block_t){CENTER + 1, 1};
    }
    
    uint8_t i;
    for (i=0; i<4; i++) {
        draw_square(blocks[i].x, blocks[i].y);
    }
}

static void draw_square(uint8_t x, uint8_t y) {
    Terminal_CursorXY(PLAYER1_UART, 2*x+1, y+2);
    Game_Printf("\xDB\xDB");
}

static void draw_blank(uint8_t x, uint8_t y) {
    Terminal_CursorXY(PLAYER1_UART, 2*x+1, y+2);
    Game_Printf("  ");
}

uint8_t tetris_move(direction_t dir) {
    tet_block_t old_blocks[4];
    tet_block_t new_blocks[4];
    uint8_t blocked = 0;
    
    uint8_t i;
    for (i=0; i<4; i++) {
        old_blocks[i] = blocks[i];
        new_blocks[i] = blocks[i];
        switch (dir) {
            case UP:    new_blocks[i].y--; break;
            case DOWN:  new_blocks[i].y++; break;
            case LEFT:  new_blocks[i].x--; break;
            case RIGHT: new_blocks[i].x++; break;
        }
        if (new_blocks[i].x >= SCREENWIDTH || new_blocks[i].y >= SCREENHEIGHT)
            blocked = 1;
        if (full_tiles[new_blocks[i].y] & 1 << new_blocks[i].x)
            blocked = 1;
    }
    if (blocked) {
        return 1;
    } else {
        for (i=0; i<4; i++)
            draw_blank(old_blocks[i].x, old_blocks[i].y);
        for (i=0; i<4; i++) {
            draw_square(new_blocks[i].x, new_blocks[i].y);
            blocks[i] = new_blocks[i];
        }
    }
    return 0;
}

static void step_down(void) {
    uint8_t status = tetris_move(DOWN);
    if (status)
        lock_in();
}

static void lock_in(void) {
    uint8_t redraw = 0;
    uint8_t i, j, rowfull;
    for (i=0; i<4; i++)
        full_tiles[blocks[i].y] |= 1 << blocks[i].x;
    
    for (i=0; i<4; i++) {
        rowfull = 1;
        while (rowfull) {
            if (full_tiles[blocks[i].y] == 0x03FF) {
                for (j = blocks[i].y; j; j--) {
                    full_tiles[j] = full_tiles[j-1];
                }
                full_tiles[0] = 0;

                if (blocks[i].y > redraw)
                    redraw = blocks[i].y;
            } else {
                rowfull = 0;
            }
        }
    }
    
    uint8_t x, y;
    if (redraw) {
        for (y = redraw; y; y--) {
            for (x = 0; x < SCREENWIDTH; x++) {
                if (full_tiles[y] & 1 << x)
                    draw_square(x, y);
                else
                    draw_blank(x, y);
            }
        }
    }
    
    speed *= 0.99;
    Task_Remove(step_down, 0);
    Task_Schedule(step_down, 0, speed, speed);
    new_block();
}

void tetris_rotate(direction_t dir) {
    tet_block_t old_blocks[4];
    tet_block_t new_blocks[4];
    uint8_t blocked = 0;
    int8_t diffx, diffy;
    
    uint8_t i;
    for (i=0; i<4; i++) {
        old_blocks[i] = blocks[i];
        new_blocks[i] = blocks[i];
    }
    
    for (i=1; i<4; i++) {
        diffx = old_blocks[i].x - old_blocks[0].x;
        diffy = old_blocks[i].y - old_blocks[0].y;
        
        switch (dir) {
            case LEFT:
                new_blocks[i].x = old_blocks[0].x + diffy;
                new_blocks[i].y = old_blocks[0].y - diffx;
                break;
            case RIGHT:
                new_blocks[i].x = old_blocks[0].x - diffy;
                new_blocks[i].y = old_blocks[0].y + diffx;
                break;
        }
        if (new_blocks[i].x >= SCREENWIDTH || new_blocks[i].y >= SCREENHEIGHT)
            blocked = 1;
        if (full_tiles[new_blocks[i].y] & 1 << new_blocks[i].x)
            blocked = 1;
    }
    if (!blocked) {
        for (i=1; i<4; i++)
            draw_blank(old_blocks[i].x, old_blocks[i].y);
        for (i=1; i<4; i++) {
            draw_square(new_blocks[i].x, new_blocks[i].y);
            blocks[i] = new_blocks[i];
        }
    }
}

///@}



