/** \defgroup average32
 * Computes the average (statistical mean) of a variable number of 32-bit
 * signed integers
 * 
 * See \ref Average32()
 * 
 * \author Josh Klodnicki
 * @{
 */

#ifndef AVERAGE32_H
#define AVERAGE32_H

#include <stdint.h>

/** Computes the average (statistical mean) of a variable number of 32-bit
 *  signed integers
 * 
 * @param   n_args Number of input arguments (excluding this one)
 * @param   ...    Comma-seperated list of 32-bit integers to average
 * @returns The average of the input arguments
 * 
 * \author  Josh Klodnicki
 * 
 * #### Example Usage ####
 * 
 * \code{.c}
 * int32_t a, b, c, average;
 * a = 1;
 * b = 5;
 * c = 6;
 * average = Average32(3, a, b, c);
 * // average = 4
 * \endcode
 */
int32_t Average32(uint8_t n_args, ...);

#endif

///@}

