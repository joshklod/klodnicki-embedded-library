/** \ingroup average32
 * @{
 */

#include "average32.h"
#include <stdint.h>
#include <stdarg.h>

int32_t Average32(uint8_t n_args, ...) {
    uint8_t i;
    int32_t sum = 0;
    
    // Extract each argument and add to sum
    va_list args;
    va_start(args, n_args);
    for (i=n_args; i; i--)
        sum += va_arg(args, int32_t);
    va_end(args);
    
    return sum/n_args;
}

///@}

