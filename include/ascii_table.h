/** \defgroup ascii_table ASCII Table
 * Pretty-prints an ASCII Table
 *
 * \author Josh Klodnicki
 *
 * Created on March 8, 2016, 12:41 AM
 *
 * \todo Document this module
 * @{
 */

#ifndef ASCII_TABLE_H
#define	ASCII_TABLE_H

void ascii_init(void);

#endif	/* ASCII_TABLE_H */

///@}

