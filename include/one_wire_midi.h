/** \defgroup one_wire_midi 1-Wire MIDI
 * Enable MIDI messages to be sent over a 1-Wire interface.
 * 
 * Call OneWireMIDI_Init() to initialize the interface. MIDI messages can then
 * be sent with the standard commands documented in the MIDI module of the
 * Embeddedlibrary.
 * 
 * \pre    Include the following Embeddedlibrary modules in system.h:
 *           - OneWire
 *           - MIDI
 * 
 * \author Josh Klodnicki
 * @{
 */

#ifndef ONE_WIRE_MIDI_H
#define	ONE_WIRE_MIDI_H

/** Initializes the OneWireMIDI module
 * 
 * This function must be called before MIDI messages can be sent over the
 * 1-Wire interface.
 * 
 * \author Josh Klodnicki
 */
void OneWireMIDI_Init(void);

#endif	/* ONE_WIRE_MIDI_H */

///@}


