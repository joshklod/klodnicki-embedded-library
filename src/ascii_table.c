/** \ingroup ascii_table
 */

#include "system.h"
#include "ascii_table.h"

#define ASCII_VERSION (version_t)0x01030002u

static void callback(int argc, char *argv[]);
static void show_help(int argc, char *argv[]);
static void error_msg(char *str);
static void print_table(int argc, char *argv[]);

static uint8_t sys_id;

void ascii_init(void) {
    sys_id = Subsystem_Init("ascii", ASCII_VERSION, callback);
}

static void callback(int argc, char *argv[]) {
    enum {HELP, TABLE, DEFAULT} mode;
    // Get mode from first argument
    if(argc) {
        if      (strcasecmp(argv[0], "help") == 0)  mode = HELP;
        else if (strcasecmp(argv[0], "table") == 0) mode = TABLE;
        else                                        mode = DEFAULT;
    }
    
    // Process command
    if(argc) {
        switch (mode) {
            case HELP:
                show_help(argc, argv);
                break;
            case TABLE:
                print_table(argc, argv);
                break;
            case DEFAULT:
                error_msg(argv[0]);
                break;
            default:
                LogMsg(sys_id, "Error processing command.");
        }
    } else {
        show_help(argc, argv);
    }
}

static void show_help(int argc, char *argv[]) {
    LogStr("\r\nASCII Table Subsys\r\n");
    LogStr("\tPretty-prints a table of ASCII special characters\r\n\n");
    LogStr("Commands:\r\n");
    LogStr("\t$ascii table [format] - Prints the ASCII Table\r\n");
    LogStr("\t                        [format] can be \"decimal\" (\"dec\")");
    LogStr(     " or \"hex\".\r\n");
    LogStr("\t$ascii help           - Show this help text\r\n");
}

static void error_msg(char *str) {
    LogStr("Unrecognized command \"%s\".\r\n", str);
    LogStr("Use \"$ascii help\" to show a list of available commands.\r\n");
}

static void print_table(int argc, char *argv[]) {
    enum {DECIMAL, HEX, DEFAULT} mode;
    // Get mode from second argument
    if(argc > 1) {
        if      (strcasecmp(argv[1], "hex") == 0)     mode = HEX;
        else if (strcasecmp(argv[1], "dec") == 0)     mode = DECIMAL;
        else if (strcasecmp(argv[1], "decimal") == 0) mode = DECIMAL;
        else                                          mode = DEFAULT;
    } else {
        mode = DECIMAL;
    }
    
    uint8_t i, j, k, c, cellwidth;
    
    switch (mode) {
        case DECIMAL: cellwidth = 7; break;
        case HEX:     cellwidth = 8; break;
        case DEFAULT:
            error_msg(argv[1]);
            return;
        default:
            LogMsg(sys_id, "Error processing command.");
    }
    
    // First Row
    LogStr("\r\n%c", 218); // TL Corner
    for (i=8; i; i--) {
        // One Cell
        for (j=cellwidth; j; j--) {
            LogStr("%c", 196); // Horizontal Line
        }
        if (i>1) LogStr("%c", 194); // Top Edge
        else     LogStr("%c", 191); // TR Corner
    }
    
    // Middle and Bottom Rows
    for (i=128; i<=143; i++) {
        // One row of numbers
        LogStr("\r\n%c", 179); // Vertical Line
        for(j=0; j<8; j++) {
            // One cell
            c = i + 16*j;
            switch (mode) {
                case DECIMAL:
                    LogStr(" %d %c %c", c, c, 179); // Num, Symb, Vertical Line
                    break;
                case HEX:
                    LogStr(" 0x%02x %c %c", c, c, 179); // Num, Symb, Vert Line
                    break;
                default:
                    LogMsg(sys_id, "Error processing command.");
            }
        }
        // One row of borders
        if(i<143) {
            LogStr("\r\n%c", 195); // Left Edge
            for (j=8; j; j--) {
                // One Cell
                for (k=cellwidth; k; k--) {
                    LogStr("%c", 196); // Horizontal Line
                }
                if (j>1) LogStr("%c", 197); // Intersection
                else     LogStr("%c", 180); // Right Edge
            }
        } else {
            LogStr("\r\n%c", 192); // BL Corner
            for (j=8; j; j--) {
                // One Cell
                for (k=cellwidth; k; k--) {
                    LogStr("%c", 196); // Horizontal Line
                }
                if (j>1) LogStr("%c", 193); // Bottom Edge
                else     LogStr("%c\r\n\n", 217); // BR Corner
            }
        }
    }
}

///@}



