/** \ingroup keyboard_scale
 * @{
 */

#include "system.h"
#include "keyboard_scale.h"
#include <stdint.h>
#include "midi.h"
#include "task.h"

/* Default Macro Settings */

#ifndef KB_SCALE_MIDI_CHANNEL
/** The MIDI channel on which the scale is sent (Default 1) */
#define KB_SCALE_MIDI_CHANNEL 1
#endif

#ifndef KB_SCALE_KEYMASK
/** Masks which notes on the keyboard are to be played
 * 
 * Can be overridden in system.h to play a different scale (Default CM/Am).
 * Each bit represents a chromatic note, read from right to left starting on
 * C. (b0 is C, b1 is C#, b2 is D, etc.)
 * 
 * #### Example ####
 * \code{.c}
 * // Play an F blues scale
 * #define KB_SCALE_KEYMASK 0b110100101001
 * \endcode
 */
#define KB_SCALE_KEYMASK 0b101010110101
#endif

/* Other Macro Definitions */

// The lowest note on the piano keyboard (A0)
#define START_NOTE 21
// The highest note on the piano keyboard (C8)
#define END_NOTE   108

/* Function Prototypes */

/** Plays a single MIDI note of the piano scale
 * 
 * When scheduled, plays the entire range of the piano keyboard (specified
 * notes only) and stops itself.
 * 
 * \author Josh Klodnicki
 */
static void play_note(void);

/* Function Definitions */

void Keyboard_Scale(uint32_t period) {
    static uint8_t init_flag = 0;
    // Initialize only once
    if (!init_flag) {
        // Ensure Task module is initialized
        Task_Init();
        // Ensure all MIDI notes are off
        MIDI_Panic(KB_SCALE_MIDI_CHANNEL);
        init_flag = 1;
    }
    
    // Send the first note outside the scheduled function
    MIDI_SendNote(KB_SCALE_MIDI_CHANNEL, START_NOTE, 127);
    Task_Schedule(play_note, 0, period, period);
}

static void play_note(void) {
    static uint8_t note = START_NOTE;
    
    // Turn off previous note
    MIDI_SendNoteOff(KB_SCALE_MIDI_CHANNEL, note);
    
    if (note >= END_NOTE) {
        // Stop playing notes
        Task_Remove(play_note, 0);
        
        MIDI_Panic(KB_SCALE_MIDI_CHANNEL);
        
        // Reset note to start for next call
        note = START_NOTE;
    } else {
        // Increment note until we find a 1 in keymask
        while (~KB_SCALE_KEYMASK & (1 << (++note%12)));
        
        MIDI_SendNote(KB_SCALE_MIDI_CHANNEL, note, 127);
    }
}

///@}
