/** \ingroup one_wire_midi
 * @{
 */

#include "system.h"
#include "one_wire_midi.h"
#include "midi.h"
#include <stdint.h>

/* Function Prototypes */

/** Callback for the MIDI module to send a MIDI sequence over 1-Wire
 * 
 * @param  data   Pointer to the first byte of data
 * @param  length Number of bytes of data to send
 * \author Josh Klodnicki
 */
static void midi_transmit(uint8_t *data, uint8_t length);

/* Function Definitions */

void OneWireMIDI_Init(void) {
    OneWire_Init();
    MIDI_Init(midi_transmit);
}

static void midi_transmit(uint8_t *data, uint8_t length) {
    // Set a pointer to the first byte of data
	uint8_t *byte = data;
    uint8_t i;
    for (i=length; i; i--) {
        // Send a byte of data and increment the pointer
        OneWire_SendByte(*(byte++));
    }
}

///@}







