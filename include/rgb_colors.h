/** \defgroup rgb_colors RGB Colors
 * Provides several typedefs for colors of various bit depths.
 *
 * Types provided are either RGB or ARGB (with alpha channel). ARGB colors are
 * packed with the alpha compoonent in the most significant position, adjacent
 * to the red component. (Ex. `argb_color32_t`: `0xAARRGGBB`)
 *
 *     [[Alpha][ Red ][Green][Blue ]]
 *     MSB                        LSB
 *
 * The types are named according to the convention `[a]rgb_color[`\e N`]_t`,
 * where the '`a`' is prepended with inclusion of an alpha component, and *N*
 * is the bit depth. If *N* is omitted, the default bit depth is 24-bit for
 * RGB colors, and 32-bit for ARGB colors.
 *
 * \author Josh Klodnicki
 * @{
 */

#ifndef RGB_COLORS_H
#define	RGB_COLORS_H

#include <stdint.h>

// RGB Types

typedef union {
    uint8_t hex;
    struct {
        unsigned blue  :2;
        unsigned green :2;
        unsigned red   :2;
		unsigned       :2;
    };
} rgb_color6_t;
    
typedef union {
    uint8_t hex;
    struct {
        unsigned blue  :2;
        unsigned green :3;
        unsigned red   :3;
    };
} rgb_color8_t;

typedef union {
    uint16_t hex;
    struct {
        unsigned blue  :4;
        unsigned green :4;
        unsigned red   :4;
		unsigned       :4;
    };
} rgb_color12_t;

typedef union {
    uint16_t hex;
    struct {
        unsigned blue  :5;
        unsigned green :5;
        unsigned red   :5;
		unsigned       :1;
    };
} rgb_color15_t;

typedef union {
    uint16_t hex;
    struct {
        unsigned blue  :5;
        unsigned green :6;
        unsigned red   :5;
    };
} rgb_color16_t;

typedef union {
    uint32_t hex;
    struct {
        unsigned blue  :8;
        unsigned green :8;
        unsigned red   :8;
		unsigned       :8;
    };
} rgb_color24_t;

typedef union {
    uint32_t hex;
    struct {
        unsigned blue  :10;
        unsigned green :10;
        unsigned red   :10;
		unsigned       :2;
    };
} rgb_color30_t;

typedef union {
    uint64_t hex;
    struct {
        unsigned blue  :16;
        unsigned green :16;
        unsigned red   :16;
		unsigned       :16;
    };
} rgb_color48_t;

typedef union {
    uint64_t hex;
    struct {
        unsigned blue  :21;
        unsigned green :21;
        unsigned red   :21;
		unsigned       :1;
    };
} rgb_color63_t;



// ARGB Types

typedef union {
    uint8_t hex;
    struct {
        unsigned blue  :2;
        unsigned green :2;
        unsigned red   :2;
		unsigned alpha :2;
    };
} argb_color8_t;

typedef union {
    uint16_t hex;
    struct {
        unsigned blue  :4;
        unsigned green :4;
        unsigned red   :4;
		unsigned alpha :4;
    };
} argb_color16_t;

typedef union {
    uint32_t hex;
    struct {
        unsigned blue  :8;
        unsigned green :8;
        unsigned red   :8;
		unsigned alpha :8;
    };
} argb_color32_t;

typedef union {
    uint64_t hex;
    struct {
        unsigned blue  :16;
        unsigned green :16;
        unsigned red   :16;
		unsigned alpha :16;
    };
} argb_color64_t;

// Default RGB is 24-bit
typedef rgb_color24_t   rgb_color_t;

// Default ARGB is 32-bit
typedef argb_color32_t  argb_color_t;



// Preset colors

#define COLOR_BLACK   ( (rgb_color_t)0x000000u )
#define COLOR_WHITE   ( (rgb_color_t)0xFFFFFFu )

#define COLOR_RED     ( (rgb_color_t)0xFF0000u )
#define COLOR_GREEN   ( (rgb_color_t)0x00FF00u )
#define COLOR_BLUE    ( (rgb_color_t)0x0000FFu )

#define COLOR_CYAN    (COLOR_GREEN | COLOR_BLUE )
#define COLOR_MAGENTA (COLOR_RED   | COLOR_BLUE )
#define COLOR_YELLOW  (COLOR_RED   | COLOR_GREEN)


#endif	/* RGB_COLORS_H */

///@}
