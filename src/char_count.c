/** \ingroup char_count
 * @{
 */
#include "system.h"
#include "char_count.h"
#include <stdint.h>
#include "uart.h"
#include "subsys.h"
#include "strings.h"
#include "task.h"

#define CHAR_VERSION (version_t)0x01020006u

/* Function Prototypes */

/** Subsystem callback for CHAR
 * \author Josh Klodnicki
 */
static void subsys_callback(int argc, char *argv[]);

/** UART Receiver callback function
 * 
 * Increments the appropriate count with every character
 * 
 * \param  c The character received by the UART channel
 * \author Josh Klodnicki
 */
static void uart_callback(char c);

/** Displays help info on the terminal
 * \author Josh Klodnicki
 */
static void show_help(void);

/** Resets all character counts
 * \author Josh Klodnicki
 */
static void reset(void);

/** Prints the value of each current count to the terminal
 * \author Josh Klodnicki
 */
static void print(void);

/** Prints an error message to the user for an unrecognized command
 * 
 * \param  cmd The unrecognized command string that the user typed
 * \author Josh Klodnicki
 */
static void print_badcmd_msg(char *cmd);

/* File-Scope Variables */

// Subsystem ID
static uint8_t sys_id;
// How many of each type of char have been received
static struct char_count {
    uint16_t total;
    uint16_t num;
    uint16_t small;
    uint16_t caps;
} count = {0,0,0,0};

/* Function Definitions */

void Char_Init(void) {
    // Register callback for $char ... commands
    sys_id = Subsystem_Init("CHAR", CHAR_VERSION, subsys_callback);
    // Register callback for every received UART character
    UART_RegisterReceiver(SUBSYS_UART, uart_callback);
}

static void uart_callback(char c) {
    count.total++;
    
    if      ('0' <= c && c <= '9') count.num++;
    else if ('a' <= c && c <= 'z') count.small++;
    else if ('A' <= c && c <= 'Z') count.caps++;
}

static void subsys_callback(int argc, char *argv[]) {
    enum {HELP, RESET, PRINT, BADCMD} mode;
    // Get mode from first argument
    if(argc) {
        if      (strcasecmp(argv[0], "help") == 0)  mode = HELP;
        else if (strcasecmp(argv[0], "reset") == 0) mode = RESET;
        else if (strcasecmp(argv[0], "print") == 0) mode = PRINT;
        else                                        mode = BADCMD;
    }
    
    // Process command
    if(argc) {
        switch (mode) {
            case HELP:
                show_help();
                break;
            case RESET:
                // Queue the task so the task management system has time to
                //   recognize the return key before executing
                Task_Queue(reset, 0);
                break;
            case PRINT:
                // Queue the task so the task management system has time to
                //   recognize the return key before executing
                Task_Queue(print, 0);
                break;
            case BADCMD:
                print_badcmd_msg(argv[0]);
                break;
            default:
                LogMsg(sys_id, "Error processing command.");
        }
    } else {
        show_help();
    }
}

static void show_help(void) {
    version_t version = CHAR_VERSION;
    LogStr("\r\nchar_count v%d.%db%d by Josh Klodnicki\r\n\n",
            version.v.major, version.v.minor, version.v.build);
    LogStr("\e[1mCHAR Subsys\e[0m\r\n");
    LogStr("\tCounts the number of characters received on the terminal\r\n\n");
    LogStr("\e[1mCommands\e[0m\r\n");
    LogStr("\t$char \e[1mreset\e[0m\tReset the count to 0\r\n");
    LogStr("\t$char \e[1mprint\e[0m\tPrint the current count\r\n");
    LogStr("\t$char \e[1mhelp\e[0m \tShow this help text\r\n\n");
}

static void reset(void) {
    count = (struct char_count){0,0,0,0};
    LogStr("Char count has been reset.\r\n\n");
}

static void print(void) {
    LogStr("Total characters received: %d\r\n",   count.total);
    LogStr("Numbers received:          %d\r\n",   count.num);
    LogStr("Small letters received:    %d\r\n",   count.small);
    LogStr("Capital letters received:  %d\r\n\n", count.caps);
}

static void print_badcmd_msg(char *cmd) {
    LogStr("Unrecognized command: '%s'\r\n", cmd);
    LogStr("Use '$char help' for a list of available commands.\r\n\n");
}

///@}
