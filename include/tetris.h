/** \defgroup tetris
 * The relentless building block video puzzle.
 * 
 * #### Controls ####
 *
 * - J/L - Move piece left/right
 * - D/F - Rotate piece
 * - K   - Drop piece
 * - R   - Restart game
 * - Q   - Quit game
 *
 * #### Commands ####
 * - `$game tetris play`
 *   - Start a game of Tetris
 * - `$game tetris (quit | exit)`
 *   - Quit to the command line  
 * - `$game tetris help`
 *   - Show the help text
 * 
 * \pre    Include the following
 * [Embeddedlibrary](https://bitbucket.org/merlin17/embeddedlibrary)
 * modules in `system.h`:
 *           - Game
 *           - Task
 *           - Terminal
 * \pre    Call `tetris_init()` to initialize the game
 * 
 * \author Josh Klodnicki
 *
 * \see
 * My full collection of embedded modules can be found
 * [on BitBucket](https://bitbucket.org/joshklod/klodnicki-embedded-library
 * "Klodnicki Embedded Library").
 * @{
 */

#ifndef TETRIS_H
#define	TETRIS_H

/** A simple enumeration of the four orthogonal directions.
 * Exposed in the header for use in `tetris_move()` and `tetris_rotate()`.
 */
typedef enum {UP, DOWN, LEFT, RIGHT} direction_t;

/** Initializes the game.
 * Must be called before Tetris is recognized by the Game module.
 */
void tetris_init(void);

/** Moves the current piece one block in any direction.
 *
 * Exposed in the header to enable the user to implement their own control
 * scheme.
 *
 * \param dir The direction to move the piece
 * \returns   Whether the piece moved successfully (0) or was blocked (1)
 */
uint8_t tetris_move(direction_t dir);

/** Rotates the current piece 90 degrees in either direction
 *
 * Exposed in the header to enable the user to implement their own control
 * scheme.
 * 
 * \param dir The direction to rotate the piece (`LEFT`=counterclockwise,
 *            `RIGHT`=clockwise)
 */
void tetris_rotate(direction_t dir);

#endif	/* TETRIS_H */

///@}


