/** \defgroup keyboard_scale Keyboard Scale
 * Plays all notes on the standard piano keyboard over MIDI
 * 
 * Configurable to play any scale via macro (\ref KB_SCALE_KEYMASK).
 * 
 * \pre Include the following Embeddedlibrary modules in system.h:
 *       - Task
 *       - MIDI
 * 
 * \author Josh Klodnicki
 * @{
 */

#ifndef KEYBOARD_SCALE_H
#define	KEYBOARD_SCALE_H

#include <stdint.h>

/** Plays all notes on the standard piano keyboard over MIDI
 * 
 * \param  period Time interval between notes in milliseconds
 * \pre    Initialize the MIDI module
 * 
 * \author Josh Klodnicki
 */
void Keyboard_Scale(uint32_t period);

#endif	/* KEYBOARD_SCALE_H */

///@}
